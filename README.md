# proxy-service

A http proxy that servers a given prime number to a prime-number-server backend via gRPC

## Usage

Run "sbt run" to start the server on port 9000.
Start the prime number server as well with sbt run in the root dir of the prime number server project.
Afterwards the system should be reachable via 
GET http://localhost:9000/prime/<number>.

## Open Task / Improvements

- Default values in application.conf with potential overwrite for env variables
- CI and docker compose
- Synchronize protobuf with the prime number server

## Limitations

- only implemented for int32

## Implementation

Since I'm familiar with it, I used the Play framework in Scala as basis for the proxy service project 
in combination with the akka grpc example for play.

