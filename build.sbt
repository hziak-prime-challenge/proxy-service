import play.core.PlayVersion.akkaVersion
import play.core.PlayVersion.akkaHttpVersion
import play.grpc.gen.scaladsl.{ PlayScalaClientCodeGenerator, PlayScalaServerCodeGenerator }
import com.typesafe.sbt.packager.docker.{ Cmd, CmdLike, DockerAlias, ExecCmd }


name := "proxy-service"
organization := "biz.ziak"
version := "1.0-SNAPSHOT"
val playGrpcVersion = "0.9.1"

lazy val root = (project in file("."))
  .enablePlugins(PlayScala,SwaggerPlugin,PlayAkkaHttp2Support,AkkaGrpcPlugin)
  .settings(
    akkaGrpcGeneratedLanguages := Seq(AkkaGrpc.Scala),
    akkaGrpcExtraGenerators += PlayScalaClientCodeGenerator,
    akkaGrpcExtraGenerators += PlayScalaServerCodeGenerator,
  )
  .settings(
    dockerBaseImage := "openjdk:8-alpine",
    dockerCommands  :=
      Seq.empty[CmdLike] ++
        Seq(
          Cmd("FROM", "openjdk:8-alpine"),
          ExecCmd("RUN", "apk", "add", "--no-cache", "bash")
        ) ++
        dockerCommands.value.tail ,
    dockerAliases in Docker += DockerAlias(None, None, "play-scala-grpc-example", None),
    packageName in Docker := "play-scala-grpc-example",
  )
  .settings(
    libraryDependencies ++= CompileDeps ++ TestDeps
  )

val CompileDeps = Seq(
  guice,
  "com.lightbend.play"      %% "play-grpc-runtime"    % playGrpcVersion,
  "com.typesafe.akka"       %% "akka-discovery"       % akkaVersion,
  "com.typesafe.akka"       %% "akka-http"            % akkaHttpVersion,
  "com.typesafe.akka"       %% "akka-http-spray-json" % akkaHttpVersion,
  // Test Database
  "com.h2database" % "h2" % "1.4.199"
)

val playVersion = play.core.PlayVersion.current
val TestDeps = Seq(
  "com.lightbend.play"      %% "play-grpc-scalatest" % playGrpcVersion % Test,
  "com.lightbend.play"      %% "play-grpc-specs2"    % playGrpcVersion % Test,
  "com.typesafe.play"       %% "play-test"           % playVersion     % Test,
  "com.typesafe.play"       %% "play-specs2"         % playVersion     % Test,
  "org.scalatestplus.play"  %% "scalatestplus-play"  % "5.0.0" % Test,
)
scalaVersion := "2.12.13"
scalacOptions ++= List("-encoding", "utf8", "-deprecation", "-feature", "-unchecked")


libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test
swaggerDomainNameSpaces := Seq("models")

testOptions in Test := Seq(Tests.Argument(TestFrameworks.JUnit, "-a", "-v"))
