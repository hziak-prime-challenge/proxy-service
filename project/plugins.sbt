val playGrpcV = "0.9.1"
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.8")
addSbtPlugin("com.lightbend.akka.grpc" %% "sbt-akka-grpc" % "1.0.2")
addSbtPlugin("com.iheart" % "sbt-play-swagger" % "0.10.6-PLAY2.8")
libraryDependencies += "com.lightbend.play" %% "play-grpc-generators" % playGrpcV
