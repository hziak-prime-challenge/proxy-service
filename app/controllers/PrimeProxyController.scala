package controllers

import biz.ziak.primserver.{PrimeRequest, PrimeServiceClient}

import javax.inject._
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

/**
 * The Main Prime Proxy Controller
 */
@Singleton
class PrimeProxyController @Inject()(implicit primeClient: PrimeServiceClient,
                                     exec: ExecutionContext,
                                     val controllerComponents: ControllerComponents
                                    ) extends BaseController {

  /**
   * Checks if the input is valid and delegates the given number back to the Prime-number-server
   */
  def getPrimes(number: Int): Action[AnyContent] = Action.async {
    implicit request: Request[AnyContent] =>
      primeClient.getPrimeNumbers(PrimeRequest(number)).flatMap {
        reply =>
          Future.successful(Ok(reply.primes.sorted.mkString(",")))
      }
  }
}
