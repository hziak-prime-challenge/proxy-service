package controllers

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.grpc.GrpcClientSettings
import biz.ziak.primserver.PrimeServiceClient
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.test._
import play.api.test.Helpers._
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Small integration test for the prime proxy
 * NOTE: For most tests the prime server has to be configured correctly and be reachable
 */
class PrimeProxyControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {
  implicit val clientSystem: ActorSystem[_] = ActorSystem(Behaviors.empty, "PrimeServiceClient")
  val grpcSettings = GrpcClientSettings.fromConfig("PrimeService").withTls(false)
  implicit val primeServiceClient = PrimeServiceClient(grpcSettings)
  implicit val contComponents = stubControllerComponents()

  "PrimeProxyController GET /prime/:number" should {

    "returning an empty string for input 0" in {
      val controller = new PrimeProxyController()
      val home = controller.getPrimes(0).apply(FakeRequest(GET, "/prime/0"))
      status(home) mustBe OK
      contentType(home) mustBe Some("text/plain")
      contentAsString(home) must be("")
    }

    "returning an empty string for input -100" in {
      val controller = new PrimeProxyController()
      val home = controller.getPrimes(-100).apply(FakeRequest(GET, "/prime/-100"))
      status(home) mustBe OK
      contentType(home) mustBe Some("text/plain")
      contentAsString(home) must be("")
    }


    /*
     * Testing the edge case that the actually send number has to be in the result aswell
     */
    "should return 2,3,5,7" in {
      val controller = new PrimeProxyController()
      val home = controller.getPrimes(7).apply(FakeRequest(GET, "/prime/7"))
      status(home) mustBe OK
      contentType(home) mustBe Some("text/plain")
      contentAsString(home) must be("2,3,5,7")
    }

    "should return 2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97" in {
      val controller = new PrimeProxyController()
      val home = controller.getPrimes(100).apply(FakeRequest(GET, "/prime/100"))
      status(home) mustBe OK
      contentType(home) mustBe Some("text/plain")
      contentAsString(home) must be("2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97")
    }

  }
}
